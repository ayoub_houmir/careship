<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 28/05/2018
 * Time: 00:19
 */

namespace tests;

use PHPUnit\Framework\TestCase;

use Slim\Http\Environment;
use Slim\Http\Request;
use Slim\Http\Response;
use src\controllers\ApiController;
use src\MyApp;

require_once "../config.php";

class ApiTest extends TestCase
{

    protected $app;

    /**
     * init MyApp
     */
    public function setUp()
    {
        $this->app = (new MyApp())->get();
    }




    /**
     *
     */
    public function testSuccessfulWithdrawMyApp()
    {
        $env = Environment::mock([
            'SCRIPT_NAME' => '/public/index.php',
            'REQUEST_METHOD' => 'GET',
            'REQUEST_URI'    => '/public/api/withdraw/100',
        ]);


        $req = Request::createFromEnvironment($env);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run();

        $this->assertSame($response->getStatusCode(), 200);

    }

    /**
     *
     */
    public function testUnsuccessfulWithdrawMyApp()
    {
        $env = Environment::mock([
            'SCRIPT_NAME' => '/public/index.php',
            'REQUEST_METHOD' => 'GET',
            'REQUEST_URI'    => '/public/api/withdraw/-140',
        ]);

        $req = Request::createFromEnvironment($env);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run();

        $this->assertSame($response->getStatusCode(), 500);


    }
}
