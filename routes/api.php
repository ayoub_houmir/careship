<?php


$app->group('/api', function() use ($app) {
    $app->get('/withdraw/{amount}',  'src\controllers\ApiController::withdraw')
        //middleware is invoked only if its route matches the current HTTP request method and URI
        ->add(new \middlewares\ResponseFormaterMiddleware());
});
