**Conception draft**

My draft present my quick analyse and is not UML standard.

` https://www.lucidchart.com/invitations/accept/16c7b74d-2048-4922-aca7-d7643e688eb9 `


**Tools**

Please, see composer.json

_Slim3: micro framework:_ help me to implement psr7 and routes faster and give me the possibility to set my own project architecture.

_monolog:_  To Write log file

_whoops:_ error handler framework


**Folders & architecture**

I try to set simple folders classifications, allow developers quick find the right place,  I set this architecture, and can be easily changed or more refactored.

`routes` I create api.php route that config api's to route the uri to the right controller, can add web route, or mobile routes ...  (Route component is implemented by SlimFramework) 

`src`  Classes and objects related to my project, only controllers is on sub-folder that allow better classified of controllers. 

`middlewares`  Midllewares, i create just two middlewares, that i think can be useful, the first react on the request object, the second one the response object.

`public` the input of the app, that init autoloader , config files, Slim app by calling src/myApp.php , suite ... help to not expose the full code on the public, web servers will point to this folder.

**Patterns & PSR**

SOLID principles - POO - DDD - TDD - DI - DIC - Middleware. _Patterns not fully implemented._
- PSR-2 (coding standard)
- PSR-4 (autoloader)
- PSR-7 (Request/Response Interface)
- PSR-11 (Middleware)


**Optimisation**

- Call logger/monolog (info) by event listener class and not a middleware.
- Add DBAL/ORM for persistence as Doctrine or Eloquent to persist current stack.
- Set cache systems Ex: "Redis" for current notes stock available.

**run**

`php -S localhost:1234`

**test**

`cd tests`

`php ../vendor/phpunit/phpunit/phpunit --no-configuration tests\ApiTest ApiTest.php --teamcity --color`

**URL**

- http://ec2-34-201-109-79.compute-1.amazonaws.com/public/api/withdraw/

**Ex**

- http://ec2-34-201-109-79.compute-1.amazonaws.com/public/api/withdraw/1000
- http://ec2-34-201-109-79.compute-1.amazonaws.com/public/api/withdraw/-140

