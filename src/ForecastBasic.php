<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 27/05/2018
 * Time: 02:45
 */

namespace src;


/**
 * basic Forecast: simple way logic how mutch notes set on ATM
 * must be related to log files or other BI solution
 *
 * in our case to make think simple we implement on paramenter how mutch of money need to retrait
 *
 * Class ForecastBasic
 * @package src
 */
class ForecastBasic extends Forecast
{

    public function forecastLogic()
    {
        return new NoteList();
    }
}