<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 27/05/2018
 * Time: 02:45
 */

namespace src;


/**
 * Forcast implement logic how mutch to add
 *
 * we init by requested amount just to make it simple because stock is illimited
 * Interface Forecast
 * @package src
 */
abstract class Forecast
{

    private $amount;
    private $cashStock;

    public function __construct(int $amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    abstract public function forecastLogic();
}