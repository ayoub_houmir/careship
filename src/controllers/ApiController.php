<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 27/05/2018
 * Time: 16:59
 */

namespace src\controllers;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;


use src\CashStack;
use src\ForecastBasic;
use src\NoteList;
use src\Withdrawal;


class ApiController
{

    /**
     * @param RequestInterface $req
     * @param ResponseInterface $response
     * @param $args
     * @return int
     */
    public static function withdraw(RequestInterface $req, ResponseInterface $response, $args)  {


        //amount validation by ValidatorMiddleware
        $amount = intval($args['amount']);

        //jsonArray
        $dbInstance = new \JSONArray();

        //init cash stock
        $cashStack = new CashStack($dbInstance);

        //init notes stock
        //$cashStack->initStock();

        //if cashStock is not available then recharge notes stock
        while ( $cashStack->canServe($amount) === false){
            //forecast
            $forcast = new ForecastBasic($amount);

            //estimate NotesList to add to stock : can be external (BI solutions)
            $forcastNotesList = $forcast->forecastLogic();

            //recharge stack
            $cashStack->setNotes($forcastNotesList);
        }

        $transactionWithdraw = new Withdrawal($amount, $cashStack);
        $listNotes = new NoteList( $transactionWithdraw->execute());

        $response = $response->getBody()->write($listNotes);

        return $response;
    }

}