<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 27/05/2018
 * Time: 02:24
 */

namespace src;

use JSONArray;
use src\NoteList;

class CashStack
{
    /**
     * @var array
     */
    private $stock;

    /**
     * @var JSONArray
     */
    private $db;


    /**
     * CashStack constructor.
     * @param JSONArray $db
     */
    public function __construct(JSONArray $db)
    {
        $this->db =  $db;
        $db->read(STORAGE_JSON_FILE);
        $this->stock = json_decode(json_encode($this->db->getArrayCopy()[0]), true);
        $this->initStock();

    }

    public function initStock(){
        $this->db[] = ['100' => 20, '50' => 20, '20' => 20, '10' => 20]; // set notes type dynamic on config file
        $this->db->write(STORAGE_JSON_FILE);
    }


    /**
     *
     * TODO implement logic notes stock available
     *
     * @param $amount
     */
    public function canServe(int $amount){
        $notesType = NOTES_TYPE;
        $rest = 0;
        foreach ($notesType as $key => $note){
            $rest = $amount % $note;
        }
        if ($rest === 0){
            return true;
        }
        return false;
    }


    // Agent Deposit
    public function setNotes($notes){
        $this->stock[] = $notes;
        $this->db[] = $notes;
        //persist
        //$this->db->write(STORAGE_JSON_FILE);
    }



}