<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 27/05/2018
 * Time: 02:26
 */

namespace src;

use src\Note;

class NoteList
{
    /**
     * @var array
     */
    private $notes = [];


    /**
     * NoteList constructor.
     * @param $notes
     */
    public function __construct($notes)
    {
        $this->notes = $notes;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $result = [];
       foreach ($this->notes as $noteValue => $count){
           for ($counter = 0 ;$counter< $count; $counter++){
               $result[] = number_format(floatval($noteValue), 2, '.', '');
           }
       }
        return implode(", ", $result);
    }

}