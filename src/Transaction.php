<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 27/05/2018
 * Time: 02:32
 */

namespace src;


abstract class Transaction
{
    protected $amount;
    private $cashStock;

    /**
     * Transaction constructor.
     * @param int $amount
     */
    public function __construct(int $amount, CashStack $cashStack)
    {
        $this->amount = $amount;
        $this->cashStock = $cashStack;
    }

    abstract public function execute();

}