<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 27/05/2018
 * Time: 02:40
 */

namespace src;


class Withdrawal extends Transaction
{

    public function execute()
    {
        $notesType = NOTES_TYPE;
        $amount = $this->amount;
        $rest = [];
        foreach ($notesType as $key => $note){
            $rest[$note] = (int) ($amount / $note);
            $amount = $amount - $rest[$note] * $note;
            //$amount = $this->amount % $note;
        }
        return $rest;
    }
}