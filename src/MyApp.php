<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 28/05/2018
 * Time: 00:26
 */

namespace src;


use Psr\Http\Message\ServerRequestInterface;
use Slim\App;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

//for debug
use Zeuxisoo\Whoops\Provider\Slim\WhoopsMiddleware;


class MyApp
{

    /**
     * Stores an instance of the Slim application.
     *
     * @var \Slim\App
     */
    private $app;

    /**
     * App constructor.
     */
    public function __construct() {

        $app = new App;


        //init app
        $app = new App(['settings' => [
            'debug' => true,                                    // Enable whoops
            'displayErrorDetails' => true,                      // Display call stack in orignal slim error when debug is off
            'determineRouteBeforeAppMiddleware' => true,        // init request route {val} before run the middleware

        ]]);

        // Fetch DI Container
        $container = $app->getContainer();

        // Register Monolog helper: using Closure
        $container['logger'] = function ($c) {
            $settings = [
                'directory' => '../storage/log',
                'filename' => 'log',
                'timezone' => 'Europe/Berlin',
                'level' => 'debug',
                'handlers' => [],
            ];
            return new \Projek\Slim\Monolog('applog', $settings);
        };

        // an example log middleware
        $app->add(function (ServerRequestInterface $req, ResponseInterface $res, $next) {
            $return = $next($req, $res);

            //log messages just for example
            $this->logger->info('Request: --request log');
            $this->logger->info('Response: -- respnse');

            return $return;
        });

        // Add whoops middleware debug
        $app->add(new WhoopsMiddleware($app));

        /**
         * formatter result : middleware is invoked only if its route matches the current HTTP request method and URI
         */
        //$app->add(new \middlewares\ResponseFormaterMiddleware());

        // validator amount request resolve return throw
        $app->add(new \middlewares\ValidatorMiddleware());

        //get api router can be controlled by uri to include the right route
        require_once '../routes/api.php';

        $this->app = $app;

    }

    /**
     * Get an instance of the application.
     *
     * @return \Slim\App
     */
    public function get()
    {
        return $this->app;
    }

}