<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 28/05/2018
 * Time: 03:27
 */

namespace middlewares;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;


class ResponseFormaterMiddleware
{
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, $next)
    {

        if ($response->getStatusCode() === 200) {
            $response->getBody()->write('[ ');
            $response = $next($request, $response);
            $response->getBody()->write(' ]');
            return $response;
        }

        return $next($response, $response);
    }
}