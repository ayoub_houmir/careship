<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 27/05/2018
 * Time: 19:04
 */

namespace middlewares;


use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;


class ValidatorMiddleware
{
    public function __invoke(ServerRequestInterface $req, ResponseInterface $rep, $next)
    {

        $getParams= $req->getAttribute('routeInfo')[2];

        if(empty($getParams['amount'])){
            $rep = $rep->withStatus(500);
            $rep->getBody()->write('[Empty Set]');
            return $rep;
        }

        $amount = intval($getParams['amount']);

        if (  $amount <= 0 )
        {
            $rep = $rep->withStatus(500);
            $rep->getBody()->write('throw InvalidArgumentException');
            return $rep;
        }

        if( ($amount % 10) !== 0 )
        {
            $rep = $rep->withStatus(500);
            $rep->getBody()->write('throw NoteUnavailableException');
            return $rep;
        }

        return  $next($req, $rep);
    }
}