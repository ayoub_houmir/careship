<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 27/05/2018
 * Time: 07:28
 */

require_once "../vendor/autoload.php";
require_once  "../config.php";


use src\MyApp;

$app = (new MyApp())->get();

$app->run();


